/*
 * DynamoDBKeyAccessor.java
 * Copyright 2018 thomas@acrosome.com
 */

package com.acrosome.ignite;

import java.io.Serializable;
import java.lang.reflect.Field;

class DynamoDBKeyAccessor<K, V> implements Serializable
{
	private static final long serialVersionUID = -4936450140667426059L;

	private final Class<K> keyClass;
	private final String hashKeyName;
	private final String sortKeyName;
	private final Field keyHashKeyField;
	private final Field keySortKeyField;
	private final Field valueHashKeyField;
	private final Field valueSortKeyField;

	DynamoDBKeyAccessor(DynamoDBTableDescriptor tableDesc, Class<K> keyClass, Class<V> valueClass) throws NoSuchFieldException, SecurityException
	{
		hashKeyName = tableDesc.getHashKey().getName();
		sortKeyName = tableDesc.hasSortKey()? tableDesc.getSortKey().getName(): null;

		if ( keyClass != null )
		{
			keyHashKeyField = keyClass.getDeclaredField(hashKeyName);
			keyHashKeyField.setAccessible(true);
			keySortKeyField = keyClass.getDeclaredField(sortKeyName);
			keySortKeyField.setAccessible(true);
		}
		else
		{
			keyHashKeyField = null;
			keySortKeyField = null;
		}
		valueHashKeyField = valueClass.getDeclaredField(hashKeyName);
		valueHashKeyField.setAccessible(true);
		if ( tableDesc.hasSortKey() )
		{
			valueSortKeyField = valueClass.getDeclaredField(sortKeyName);
			valueSortKeyField.setAccessible(true);
		}
		else
		{
			valueSortKeyField = null;
		}
		this.keyClass = keyClass;
	}

	Object getHashKeyValueFromKeyObject(K k)
	{
		return getFieldValue(keyHashKeyField, k);
	}

	Object getSortKeyValueFromKeyObject(K k)
	{
		return getFieldValue(keySortKeyField, k);
	}

	@SuppressWarnings("unchecked")
	K getKeyFromValueObject(V v) throws InstantiationException, IllegalAccessException
	{
		if ( keyClass == null )
		{
			return (K) getHashKeyValueFromValueObject(v);
		}
		else
		{
			K k = keyClass.newInstance();
			keyHashKeyField.set(k, getHashKeyValueFromValueObject(v));
			keySortKeyField.set(k, getSortKeyValueFromValueObject(v));
			return k;
		}
	}

	private Object getHashKeyValueFromValueObject(V v)
	{
		return getFieldValue(valueHashKeyField, v);
	}

	private Object getSortKeyValueFromValueObject(V v)
	{
		return getFieldValue(valueSortKeyField, v);
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("DynamoDBKeyAccessor [keyClass=");
		builder.append(keyClass);
		builder.append(", hashKeyName=");
		builder.append(hashKeyName);
		builder.append(", sortKeyName=");
		builder.append(sortKeyName);
		builder.append(", keyHashKeyField=");
		builder.append(keyHashKeyField);
		builder.append(", keySortKeyField=");
		builder.append(keySortKeyField);
		builder.append(", valueHashKeyField=");
		builder.append(valueHashKeyField);
		builder.append(", valueSortKeyField=");
		builder.append(valueSortKeyField);
		builder.append("]");
		return builder.toString();
	}

	private static final Object getFieldValue(Field field, Object obj)
	{
		try
		{
			return field.get(obj);
		}
		catch ( IllegalAccessException e )
		{
			throw new IllegalArgumentException(field.getName() + " field is inaccessible from value object:" + obj, e);
		}
	}
}
;