/*
 * DynamoDBStoreFactory.java
 * Copyright 2018 thomas@acrosome.com
 */

package com.acrosome.ignite;

import java.util.Objects;

import javax.cache.configuration.Factory;

import org.apache.ignite.IgniteLogger;
import org.apache.ignite.resources.LoggerResource;

public class DynamoDBStoreFactory<K, V> implements Factory<DynamoDBStore<K, V>> 
{
	private static final long serialVersionUID = 19176972447761147L;

	@LoggerResource
	private IgniteLogger log;

	private DynamoDBDataSource dataSource;
	private String tableName;
	private String keyClass;
	private String valueClass;

	@Override
	public DynamoDBStore<K, V> create()
	{
		log.debug("Create DynamoDBStore config:" + this);
		try
		{
			Objects.requireNonNull(dataSource, "dataSource is null");
			Objects.requireNonNull(tableName, "tableName is null");
			Objects.requireNonNull(valueClass, "valueClass is null");

			return new DynamoDBStore<K, V>(dataSource, tableName, keyClass, valueClass);
		}
		catch ( ClassNotFoundException | NoSuchFieldException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	public DynamoDBDataSource getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(DynamoDBDataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	public String getTableName()
	{
		return tableName;
	}

	public void setTableName(String tableName)
	{
		this.tableName = tableName;
	}

	public String getValueClass()
	{
		return valueClass;
	}

	public void setValueClass(String valueClass)
	{
		this.valueClass = valueClass;
	}

	public String getKeyClass()
	{
		return keyClass;
	}

	public void setKeyClass(String keyClass)
	{
		this.keyClass = keyClass;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("DynamoDBStoreFactory [log=");
		builder.append(log);
		builder.append(", dataSource=");
		builder.append(dataSource);
		builder.append(", tableName=");
		builder.append(tableName);
		builder.append(", keyClass=");
		builder.append(keyClass);
		builder.append(", valueClass=");
		builder.append(valueClass);
		builder.append("]");
		return builder.toString();
	}
}
;