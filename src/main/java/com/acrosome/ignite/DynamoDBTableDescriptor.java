/*
 * DynamoDBTableDescriptor.java
 * Copyright 2019 thomas@acrosome.com
 */

package com.acrosome.ignite;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;

final class DynamoDBTableDescriptor implements Serializable
{
	private static final long serialVersionUID = -614434289678882150L;

	private final String tablename;
	private final DynamoDBKey hashKey;
	private final DynamoDBKey sortKey;
	private final boolean hasSortKey;

	public DynamoDBTableDescriptor(TableDescription tableDesc)
	{
		tablename = tableDesc.getTableName();
		Map<String, DynamoDBKeyType> typeMap = StreamSupport.stream(tableDesc.getAttributeDefinitions().spliterator(), false)
		.collect(Collectors.toMap(e -> 
		{
			return e.getAttributeName();
		}, e ->
		{
			return DynamoDBKeyType.fromString(e.getAttributeType());
		}));
		DynamoDBKey tempHaskKey = null, tempSortKey = null;
		if ( typeMap.size() == 1 )
		{
			Map.Entry<String, DynamoDBKeyType> e = typeMap.entrySet().iterator().next();
			tempHaskKey = new DynamoDBKey(e.getKey(), e.getValue(), true);
			tempSortKey = null;
		}
		else
		{
			for ( KeySchemaElement e : tableDesc.getKeySchema() )
			{
				if ( KeyType.HASH.toString().equals(e.getKeyType()) )
				{
					String name = e.getAttributeName();
					tempHaskKey = new DynamoDBKey(name, typeMap.get(name), true);
				}
				else if ( KeyType.RANGE.toString().equals(e.getKeyType()) )
				{
					String name = e.getAttributeName();
					tempSortKey = new DynamoDBKey(name, typeMap.get(name), false);
				}
			}
		}
		hashKey = tempHaskKey;
		sortKey = tempSortKey;
		hasSortKey = Objects.nonNull(sortKey);
	}

	String getTablename()
	{
		return tablename;
	}

	boolean hasSortKey()
	{
		return hasSortKey;
	}

	DynamoDBKey getHashKey()
	{
		return hashKey;
	}

	/** return null if the table doesn't has sort key. <code>hasSortKey()</code> return false.*/
	DynamoDBKey getSortKey()
	{
		return sortKey;
	}

	String getHashKeyName()
	{
		return hashKey.getName();
	}

	/** return null if the table doesn't has sort key. <code>hasSortKey()</code> return false.*/
	String getSortKeyName()
	{
		return sortKey != null? sortKey.getName(): null;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("DynamoDBTableDescriptor [tablename=");
		builder.append(tablename);
		builder.append(", hashKey=");
		builder.append(hashKey);
		builder.append(", sortKey=");
		builder.append(sortKey);
		builder.append(", hasSortKey=");
		builder.append(hasSortKey);
		builder.append("]");
		return builder.toString();
	}
};