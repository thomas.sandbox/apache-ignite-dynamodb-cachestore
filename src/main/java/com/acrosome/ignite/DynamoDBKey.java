/*
 * DynamoDBKey.java
 * Copyright 2019 thomas@acrosome.com
 */

package com.acrosome.ignite;

import java.io.Serializable;

class DynamoDBKey implements Serializable
{
	private static final long serialVersionUID = -5984949145810695337L;

	private String name;
	private DynamoDBKeyType type;
	private boolean isHashKey;

	public DynamoDBKey(String name, DynamoDBKeyType type, boolean isHashKey)
	{
		this.name = name;
		this.type = type;
		this.isHashKey = isHashKey;
	}

	public String getName()
	{
		return name;
	}

	public DynamoDBKeyType getType()
	{
		return type;
	}

	public boolean isHashKey()
	{
		return isHashKey;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("DynamoDBKey [name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(", isHashKey=");
		builder.append(isHashKey);
		builder.append("]");
		return builder.toString();
	}

};