/*
 * DynamoDBDataSource.java
 * Copyright 2018 thomas@acrosome.com
 */

package com.acrosome.ignite;

import java.io.Serializable;

public class DynamoDBDataSource implements Serializable
{
	private static final long serialVersionUID = 4057963794582722529L;

	private String region;
	private String accessKey;
	private String secretKey;
	private String sessionToken;

	public String getRegion()
	{
		return region;
	}

	public void setRegion(String region)
	{
		this.region = region;
	}

	public String getAccessKey()
	{
		return accessKey;
	}

	public void setAccessKey(String accessKey)
	{
		this.accessKey = accessKey;
	}

	public String getSecretKey()
	{
		return secretKey;
	}

	public void setSecretKey(String secretKey)
	{
		this.secretKey = secretKey;
	}

	public String getSessionToken()
	{
		return sessionToken;
	}

	public void setSessionToken(String sessionToken)
	{
		this.sessionToken = sessionToken;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("DynamoDBDataSource [region=");
		builder.append(region);
		builder.append(", accessKey=");
		builder.append(accessKey);
		builder.append(", secretKey=");
		builder.append(secretKey);
		builder.append(", sessionToken=");
		builder.append(sessionToken);
		builder.append("]");
		return builder.toString();
	}
};