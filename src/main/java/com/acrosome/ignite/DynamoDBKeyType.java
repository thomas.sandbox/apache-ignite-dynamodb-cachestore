/*
 * DynamoDBKeyType.java
 * Copyright 2019 thomas@acrosome.com
 */

package com.acrosome.ignite;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

enum DynamoDBKeyType
{
	STRING,
	NUMBER,
	BINARY
	;

	public static final DynamoDBKeyType fromString(String s)
	{
		switch (s)
		{
			case "S": return DynamoDBKeyType.STRING;
			case "N": return DynamoDBKeyType.NUMBER;
			case "B": return DynamoDBKeyType.BINARY;
			default: throw new IllegalArgumentException("unknown getAttributeType:" + s);
		}
	}

	public Object getValueFromAttributeValue(AttributeValue value)
	{
		switch (this)
		{
			case STRING: return value.getS();
			case NUMBER: return value.getN().indexOf('.') == -1? Long.parseLong(value.getN()): Double.parseDouble(value.getN());
			case BINARY: return value.getB();
			default: throw new IllegalArgumentException("Unknown KeyDataType for value:" + value);
		}
	}
}
;