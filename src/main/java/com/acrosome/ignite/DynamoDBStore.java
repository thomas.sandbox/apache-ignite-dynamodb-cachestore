/*
 * IgniteDynamodbStore.java
 * Copyright 2018 thomas@acrosome.com
 */

package com.acrosome.ignite;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.cache.Cache.Entry;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;

import org.apache.ignite.IgniteLogger;
import org.apache.ignite.cache.store.CacheStore;
import org.apache.ignite.lang.IgniteBiInClosure;
import org.apache.ignite.resources.LoggerResource;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.google.gson.Gson;

public class DynamoDBStore<K, V> implements CacheStore<K, V>
{
	private static final Gson GSON = new Gson();
	private static final ClientConfiguration clientConfig = new ClientConfiguration().withGzip(true).withMaxConnections(100);

	@LoggerResource
	private IgniteLogger log;

	private final DynamoDB ddb;
	private final Table table;
	private final DynamoDBTableDescriptor tableDesc;
	private final DynamoDBKeyAccessor<K, V> keyAccessor;
	private final Class<K> keyClass;
	private final Class<V> valueClass;

	@SuppressWarnings("unchecked")
	DynamoDBStore(DynamoDBDataSource ds, String tableName, String keyClass, String valueClass) throws ClassNotFoundException, NoSuchFieldException, SecurityException
	{
		if ( ds != null )
		{
			if ( ds.getAccessKey() != null && ds.getSecretKey() != null )
			{
				if ( ds.getSessionToken() != null )
				{
					AWSCredentialsProvider provider = new AWSStaticCredentialsProvider(new BasicSessionCredentials(ds.getAccessKey(), ds.getSecretKey(), ds.getSessionToken()));
					ddb = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(ds.getRegion()).withCredentials(provider).withClientConfiguration(clientConfig).build());
				}
				else
				{
					AWSCredentialsProvider provider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(ds.getAccessKey(), ds.getSecretKey()));
					ddb = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(ds.getRegion()).withCredentials(provider).withClientConfiguration(clientConfig).build());
				}
			}
			else
			{
				// accesskey is null, use DefaultAWSCredentialsProviderChain
				AWSCredentialsProvider provider = new DefaultAWSCredentialsProviderChain();
				ddb = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(ds.getRegion()).withCredentials(provider).withClientConfiguration(clientConfig).build());
			}
		}
		else
		{
			// use DefaultAWSCredentialsProviderChain and region from aws configure
			AWSCredentialsProvider provider = new DefaultAWSCredentialsProviderChain();
			ddb = new DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(Regions.getCurrentRegion().toString()).withCredentials(provider).withClientConfiguration(clientConfig).build());
		}
		this.table = ddb.getTable(tableName);
		this.tableDesc = new DynamoDBTableDescriptor(table.describe());
		if ( tableDesc.getSortKey() != null && keyClass == null )
		{
			throw new IllegalArgumentException("compositive key require keyClass property.");
		}
		this.keyClass = keyClass != null? (Class<K>) Class.forName(keyClass): null;
		this.valueClass = (Class<V>) Class.forName(valueClass);
		this.keyAccessor = new DynamoDBKeyAccessor<K, V>(tableDesc, this.keyClass, this.valueClass);
	}

	@Override
	public void write(Entry<? extends K, ? extends V> entry) throws CacheWriterException
	{
		String json = GSON.toJson(entry.getValue(), entry.getValue().getClass());
		try
		{
			Item i = Item.fromJSON(json);
			table.putItem(i);
		}
		catch ( Exception e)
		{
			throw new CacheWriterException("write cache failed value:" + entry.getValue(), e);
		}
	}

	@Override
	public void writeAll(Collection<Entry<? extends K, ? extends V>> entries) throws CacheWriterException
	{
		if ( log.isTraceEnabled() )
		{
			log.trace("========> writeAll() entries:" + entries);
		}
		TableWriteItems items = new TableWriteItems(table.getTableName());
		int count = 0;
		for ( Entry<? extends K, ? extends V> entry : entries )
		{
			String json = GSON.toJson(entry.getValue(), entry.getValue().getClass());
			Item item = Item.fromJSON(json);
			items.addItemToPut(item);
			count++;
			if ( count >= 25 )
			{
				ddb.batchWriteItem(items);
				items = new TableWriteItems(table.getTableName());
				count = 0;
			}
		}
		if ( count > 0 )
		{
			ddb.batchWriteItem(items);
		}
	}

	@Override
	public void delete(Object key) throws CacheWriterException
	{
		if ( log.isTraceEnabled() )
		{
			log.trace("========> delete() key:" + key);
		}
		@SuppressWarnings("unchecked")
		K k = (K) key;
		if ( tableDesc.hasSortKey() )
		{
			table.deleteItem(tableDesc.getHashKeyName(), keyAccessor.getHashKeyValueFromKeyObject(k), tableDesc.getSortKeyName(), keyAccessor.getSortKeyValueFromKeyObject(k));
		}
		else
		{
			table.deleteItem(tableDesc.getHashKey().getName(), key);
		}
	}

	@Override
	public void deleteAll(Collection<?> keys) throws CacheWriterException
	{
		if ( log.isTraceEnabled() )
		{
			log.trace("========> deleteAll() keys:" + keys);
		}
		TableWriteItems items = new TableWriteItems(table.getTableName());
		int count = 0;
		for ( Object key : keys )
		{
			if ( tableDesc.hasSortKey() )
			{
				items.addPrimaryKeyToDelete(new PrimaryKey(tableDesc.getHashKey().getName(), key));
			}
			else
			{
				@SuppressWarnings("unchecked")
				K k = (K)key;
				items.addPrimaryKeyToDelete(new PrimaryKey(tableDesc.getHashKeyName(), keyAccessor.getHashKeyValueFromKeyObject(k), tableDesc.getSortKeyName(), keyAccessor.getSortKeyValueFromKeyObject(k)));
			}
			count++;
			if ( count >= 25 )
			{
				ddb.batchWriteItem(items);
				items = new TableWriteItems(table.getTableName());
				count = 0;
			}
		}
		if ( count > 0 )
		{
			ddb.batchWriteItem(items);
		}
	}

	@Override
	public V load(K key) throws CacheLoaderException
	{
		if ( log.isTraceEnabled() )
		{
			log.trace("========> load() key:" + key + " table:" + table + " keyAccessor:" + keyAccessor);
		}
		if ( tableDesc.hasSortKey() )
		{
			Item i = table.getItem(tableDesc.getHashKeyName(), keyAccessor.getHashKeyValueFromKeyObject(key), tableDesc.getSortKeyName(), keyAccessor.getSortKeyValueFromKeyObject(key));
			return i == null? null: GSON.fromJson(i.toJSON(), valueClass);
		}
		else
		{
			Item i = table.getItem(tableDesc.getHashKey().getName(), key);
			return i == null? null: GSON.fromJson(i.toJSON(), valueClass);
		}
	}

	@Override
	public Map<K, V> loadAll(Iterable<? extends K> keys) throws CacheLoaderException
	{
		if ( log.isTraceEnabled() )
		{
			log.trace("========> loadAll() keys:" + keys);
		}
		return StreamSupport.stream(keys.spliterator(), true).collect(Collectors.toMap( e ->
		{
			return e;
		}, e ->
		{
			return load(e);
		}));
	}

	@Override
	public void loadCache(IgniteBiInClosure<K, V> clo, Object... args) throws CacheLoaderException
	{
		if ( log.isTraceEnabled() )
		{
			log.trace("========> loadAll() clo:" + clo + " args:" + Arrays.toString(args));
		}
		Map<String, AttributeValue> lastEvaluateKey = null;
		ScanSpec spec = new ScanSpec().withMaxResultSize(1000);
		do
		{
			if ( lastEvaluateKey != null )
			{
				DynamoDBKey hashKey = tableDesc.getHashKey();
				Object hashKeyValue = hashKey.getType().getValueFromAttributeValue(lastEvaluateKey.get(hashKey.getName()));
				if ( tableDesc.hasSortKey() )
				{
					DynamoDBKey sortKey = tableDesc.getSortKey();
					Object sortKeyValue = sortKey.getType().getValueFromAttributeValue(lastEvaluateKey.get(sortKey.getName()));
					spec.withExclusiveStartKey(hashKey.getName(), hashKeyValue, sortKey.getName(), sortKeyValue);
				}
				else
				{
					spec.withExclusiveStartKey(hashKey.getName(), hashKeyValue);
				}
			}
			ItemCollection<ScanOutcome> ret = table.scan(spec);
			ret.forEach(i ->
			{
				V v = GSON.fromJson(i.toJSON(), valueClass);
				try
				{
					K k = keyAccessor.getKeyFromValueObject(v);
					clo.apply(k, v);
				}
				catch ( InstantiationException | IllegalAccessException e )
				{
					throw new CacheLoaderException(e);
				}
			});
			lastEvaluateKey = ret.getLastLowLevelResult().getScanResult().getLastEvaluatedKey();
		}
		while(lastEvaluateKey != null);
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("DynamoDBStore [table=");
		builder.append(table);
		builder.append(", tableDesc=");
		builder.append(tableDesc);
		builder.append(", keyAccessor=");
		builder.append(keyAccessor);
		builder.append(", keyClass=");
		builder.append(keyClass);
		builder.append(", valueClass=");
		builder.append(valueClass);
		builder.append("]");
		return builder.toString();
	}

	@Override
	@Deprecated
	public void sessionEnd(boolean commit) throws CacheWriterException
	{
//		throw new UnsupportedOperationException("sessionEnd");
	}
}
;